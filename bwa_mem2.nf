#!/usr/bin/env nextflow

process bwa_mem2_index {
// Runs bwa_mem2 index
//
// input:
//   path fa - Reference FASTA
//   val params - Additional Parameters
//
// output:
//   tuple => emit: idx_files
//     path(fa) - Reference FASTA
//     path("${fa}.*") - Index Files

// require:
//   params.bwa_mem2$dna_ref
//   params.bwa_mem2$bwa_mem2_index_parameters

  storeDir "${params.shared_dir}/${fa}/bwa_mem2_index"
  tag "${fa}"
  label 'bwa_mem2_container'
  label 'bwa_mem2_index'
  cache 'lenient'

  input:
  path fa
  val parstr

  output:
  tuple path(fa), path("${fa}.*"), emit: idx_files

  script:
  """
  bwa-mem2 index ${parstr} ${fa}
  """
}


process bwa_mem2 {
// Runs bwa_mem2
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq2) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   IDX_FILES
//   params.bwa_mem2$bwa_mem2_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'bwa_mem2_container'
  label 'bwa_mem2'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.sam'), emit: sams

  script:
  """
  bwa-mem2 mem ${parstr} ${fa} ${fq1} ${fq2} > ${dataset}-${pat_name}-${run}.sam -t ${task.cpus}
  """
}


process bwa_mem2_mem_samtools_sort {
// Runs bwa_mem2 mem piped to samtools sort (to minimize storage footprint)
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: bams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*.bam") - Output BAM File

// require:
//   FQS
//   IDX_FILES
//   params.bwa_mem2$bwa_mem2_mem_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'bwa_mem2_samtools_container'
  label 'bwa_mem2_samtools'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  tuple path(fa), path(idx_files)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*sorted.bam'), emit: bams

  script:
  """
  bwa-mem2 mem ${parstr} -R "@RG\\tID:${dataset}-${pat_name}-${run}\\tSM:${dataset}-${pat_name}-${run}\\tLB:NULL\\tPL:Illumina" ${fa} ${fq1} ${fq2} -t ${task.cpus - 1} |\
  samtools sort -@ 1 - > ${dataset}-${pat_name}-${run}.sorted.bam
  """
}
